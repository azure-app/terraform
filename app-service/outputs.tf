output "webapp_url" {
  value       = azurerm_windows_web_app.webapp.default_hostname
  description = "The webapp hostname"
}

output "resource_group_name" {
  value       = azurerm_resource_group.appservice.name
  description = "The resource group name"
}

output "webapp_name" {
  value       = azurerm_windows_web_app.webapp.name
  description = "The application service plan name"
}