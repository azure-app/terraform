resource "random_id" "id" {
  byte_length = 1
}

resource "azurerm_resource_group" "appservice" {
  name     = "${var.rg_name}-${random_id.id.hex}-rg"
  location = var.location
  # depends_on = [azurerm_resource_provider_registration.app_registration]
}

resource "azurerm_service_plan" "webapp-plan" {
  name                = "${var.webapp_plan}-${random_id.id.hex}-plan"
  location            = azurerm_resource_group.appservice.location
  resource_group_name = azurerm_resource_group.appservice.name
  sku_name            = var.sku_name
  os_type             = var.os_type
}

resource "azurerm_windows_web_app" "webapp" {
  name                = "${var.webapp_name}-${random_id.id.hex}-web-app"
  location            = azurerm_resource_group.appservice.location
  resource_group_name = azurerm_resource_group.appservice.name
  service_plan_id     = azurerm_service_plan.webapp-plan.id
  https_only          = true
  site_config {}
  app_settings = {
    "SOME_KEY" = "value"
  }
}

data "azurerm_subscription" "primary" {
}

# resource "azurerm_resource_provider_registration" "app_registration" {
#   count = length(var.applications_registration)
#   name  = "Microsoft.${var.applications_registration[count.index]}"
# }