terraform {
  required_version = ">=1.1.2"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.86.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id            = var.ARM_SUBSCRIPTION_ID
  tenant_id                  = var.ARM_TENANT_ID
  client_id                  = var.ARM_CLIENT_ID
  client_secret              = var.ARM_CLIENT_SECRET
  skip_provider_registration = true
}

provider "azuread" {
  tenant_id     = var.ARM_TENANT_ID
  client_id     = var.ARM_CLIENT_ID
  client_secret = var.ARM_CLIENT_SECRET
}