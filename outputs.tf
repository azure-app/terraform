output "webapp_url" {
  value       = module.appservice.webapp_url
  description = "The webapp hostname"
}

output "rg_name" {
  value       = module.appservice.resource_group_name
  description = "The resource group name"
}

output "webapp_name" {
  value       = module.appservice.webapp_name
  description = "The application service plan name"
}