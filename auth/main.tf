resource "azuread_application_registration" "app" {
  display_name = "App Registration ${random_id.id.hex}"
}

resource "azurerm_role_assignment" "role" {
  scope                = data.azurerm_subscription.primary.id
  role_definition_name = "Contributor"
  principal_id         = azuread_service_principal.app.object_id
}

resource "azuread_service_principal" "app" {
  client_id    = azuread_application_registration.app.client_id
  use_existing = true
}

resource "azuread_service_principal_password" "app" {
  display_name         = "Web App ${random_id.id.hex}"
  service_principal_id = azuread_service_principal.app.id
}

resource "random_id" "id" {
  byte_length = 1
}